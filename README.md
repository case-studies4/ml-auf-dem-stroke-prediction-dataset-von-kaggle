# Willkommen bei [ ML auf dem Stroke Prediction Dataset von Kaggle] 

In unserem Projekt 'Die Wahrscheinlichkeit eines Schlaganfalls aufgrund von Symptomen' untersuchen wir eingehend die Zusammenhänge zwischen verschiedenen Gesundheitssymptomen und der Wahrscheinlichkeit, einen Schlaganfall zu erleiden.
Unser Ziel ist es, wichtige Einblicke in die Risikofaktoren und Warnzeichen eines Schlaganfalls zu gewinnen, um die Früherkennung und Prävention dieser lebensverändernden Ereignisse zu verbessern. [Stroke Prediction Dataset](https://www.kaggle.com/datasets/fedesoriano/stroke-prediction-dataset/)

## Über das Projekt

Um unser Ziel zu erreichen, werden wir die folgenden Schritte durchführen:
. Datenerfassung
. Datenbereinigung und -vorverarbeitung
. Explorative Datenanalyse
. Modellentwicklung
. Validierung und Bewertung
. Ergebnisinterpretation
. Berichterstellung und Kommunikation

In diesem Projekt finden Sie zwei Dateien:

- `Preprocessing+ML.ipynb`: Hier finden Sie den gesamten Prozess von der Dateneingabe und -vorbereitung bis hin zum maschinellen Lernen für die Modellierung und Vorhersage. Dabei wurden fünf verschiedene ML-Modelle verwendet und miteinander verglichen.

- `DecisionTree_Modelierung.ipynb`: Entdecken Sie in dieser Datei eine umfassende Erklärung des Decision-Tree-Modells sowie diverse Anpassungen, die ich ausprobiert habe.